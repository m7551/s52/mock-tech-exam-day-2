function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    if(letter.length == 1){
        for(i=0; i < sentence.length; i++){
            if(letter == sentence[i]){
                result++
            }
        }
    } else {
        return undefined
    }
    return result
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    let word = text.toLowerCase()
    let nospace = word.split('')
    for(i=0; i < nospace.length; i++){
        if(nospace[i] == nospace[i+1]){
            return false
        }
    }
    return true
    // return !/(.).*\1/.test(text)
}

function purchase(age, price) {
    let discount = .2;
    // let dprice = price * discount
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    if(age > 12 && age < 22){
       let dprice = price - (price * discount)
       dprice = Math.round(dprice * 100) / 100
       return dprice.toString()
    } else if(age >=22 && age <= 64){
        price = Math.round(price * 100) / 100
        return price.toString()
    } else if (age >= 65){
        let dprice = price - (price * discount)
       dprice = Math.round(dprice * 100) / 100
       return dprice.toString()
    }
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {
    let noStock = []
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    for(let i=0; i < items.length; i++){
        if (items[i].stocks === 0){
            noStock.push(items[i])
        }
    }
     noStock = noStock.map(x => x.category)

     return noStock = [...new Set(noStock)]


}

function findFlyingVoters(candidateA, candidateB) {
    let duplicate = []
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.


    candidateA.forEach(arrA => {
        candidateB.forEach(arrB => {
            if(arrA == arrB){
                duplicate.push(arrA)
            }
        })
    })
    return duplicate
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};